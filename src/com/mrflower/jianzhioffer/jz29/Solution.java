package com.mrflower.jianzhioffer.jz29;

/**
 *
 * 输入一个矩阵，按照从外向里以顺时针的顺序依次打印出每一个数字。
 *
 * 示例 1：
 * 输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
 * 输出：[1,2,3,6,9,8,7,4,5]
 * 示例 2：
 * 输入：matrix =[[1,2,3,4],[5,6,7,8],[9,10,11,12]]
 * 输出：[1,2,3,4,8,12,11,10,9,5,6,7]
 *
 *
 *
 */
public class Solution {
    public int[] spiralOrder(int[][] matrix) {
        if(matrix.length == 0){
            return new int[0];
        }
        int left = 0;
        int right = matrix[0].length - 1;
        int top = 0;
        int bottom = matrix.length - 1;
        int[] res = new int[matrix.length * matrix[0].length];
        int index = 0;
        while (true){
            //从左到右
            for(int i = left;i<=right;i++){
                res[index] = matrix[top][i];
                index++;
            }
            //top+1，下移一行
            if(++top > bottom){
                break;
            }
            //从上到下
            for(int i = top;i<=bottom;i++){
                res[index] = matrix[i][right];
                index++;
            }
            //right-1，左移一列
            if(--right < left){
                break;
            }
            //从右到左
            for(int i = right;i>=left;i--){
                res[index] = matrix[bottom][i];
                index++;
            }
            //bottom - 1，上移一行
            if(--bottom < top){
                break;
            }

            //从下到上
            for(int i = bottom;i>=top;i--){
                res[index] = matrix[i][left];
                index++;
            }
            //left+1，右移一列
            if(++left > right){
                break;
            }

        }
        return res;
    }
}
